package com.example.api_rest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.example.api_rest.entities.Contact;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {

}
