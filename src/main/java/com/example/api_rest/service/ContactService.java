package com.example.api_rest.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.api_rest.entities.Contact;
import com.example.api_rest.repository.ContactRepository;

import lombok.Data;

@Data
@Service
public class ContactService {

    @Autowired
    private ContactRepository ContactRepository;

    public Iterable<Contact> getContacts() {
        return ContactRepository.findAll();
    }

    public Optional<Contact> getContact(final Long id) {
        return ContactRepository.findById(id);
    }

    public void deleteContactByID(final Long id) {
        ContactRepository.deleteById(id);
    }

    public Contact saveOrUpdate(Contact Contact)
    {
        return ContactRepository.save(Contact);
    }

    public void update(Contact Contact, final Long id)
    {
        ContactRepository.save(Contact);
    }

    /**
    public Contact saveContact(Contact Contact) {
        Contact savedContact = ContactRepository.save(Contact);
        return savedContact;
    }

     */



}
