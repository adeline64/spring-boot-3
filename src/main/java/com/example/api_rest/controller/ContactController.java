package com.example.api_rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.api_rest.entities.Contact;
import com.example.api_rest.service.ContactService;

import java.util.Optional;

@RestController
public class ContactController {

    @Autowired
    private ContactService ContactService;

    @GetMapping("/contacts")
    public Iterable<Contact> getContacts() {
        return ContactService.getContacts();
    }

    @GetMapping("/contact/{id}")
    public Optional<Contact> getContact( @PathVariable Long id) {
        return ContactService.getContact(id);
    }

    @DeleteMapping("/DeleteContact/{id}")
    public void deleteContactByID(@PathVariable Long id)
    {
        ContactService.deleteContactByID(id);
    }

    @PostMapping("/PostContact")
    private Contact saveContact(@RequestBody Contact contact)
    {
        return ContactService.saveOrUpdate(contact);
    }

    @PutMapping("/PutContact")
    private Contact update(@RequestBody Contact contact)
    {
        ContactService.saveOrUpdate(contact);
        return contact;
    }

}
